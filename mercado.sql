drop database if exists supermercado ;
CREATE DATABASE supermercado;
use supermercado;

CREATE TABLE producto(
id INTEGER auto_increment primary key,
nombre varchar(50),
proveedor varchar(50),
seccion varchar(80),
precio float
);

insert into producto (nombre, proveedor, seccion, precio)
values 
('Leche', 'Puleva','Lactos', 3),
('Humus', 'Hacendado','Precocinados', 2),
('Patatas Fritas', 'Matutano','Aperitivos', 4),
('Bollitos de chocolate', 'Dulcesol','Panaderia', 4),
('Ositos de gominola', 'Haribo','Dulces', 3),
('Aceite de Oliva', 'Hacendado','Aceites', 5),
('Mascarillas', 'Deliplus','Belleza', 5),
('Gel de ducha', 'Lactovit','Belleza', 2),
('Lejia', 'Estrella','Limpieza', 1),
('Comida de perro', 'Compy','Mascotas', 50);