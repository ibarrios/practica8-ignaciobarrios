package paquete;

import java.sql.SQLException;
import java.util.Scanner;

public class Menu {
	

	public static void main(String[] args) throws SQLException {
		int op;
		Scanner input = new Scanner(System.in);
		ConectandoConMercado misDatos = new ConectandoConMercado();

		do {
			op = menu();
			switch (op) {
			case 1:
				misDatos.conectar();
				break;
			case 2:
				misDatos.seleccionar();
				break;
			case 3:
				String nuevaInsercion = "";
				do {
				System.out.println("Introduce el nombre del producto");
				String nombre=input.nextLine();
				System.out.println("Introduce la proveedor del producto");
				String proveedor=input.nextLine();
				System.out.println("Introduce el seccion del producto");
				String seccion=input.nextLine();
				System.out.println("Introduce el precio del producto");
				float precio=input.nextFloat();
				misDatos.insertar(nombre,  proveedor, seccion, precio);
				misDatos.seleccionar();
				input.nextLine();
				System.out.println("Quieres introducir otro productor? Introduce si o no");
				nuevaInsercion = input.nextLine();
				} while (nuevaInsercion.toLowerCase().contentEquals("si"));
				break;
			case 4:
				System.out.println("Introduce el nombre del producto");
				String nombre=input.nextLine();
				System.out.println("Introduce la proveedor del producto");
				String proveedor=input.nextLine();
				System.out.println("Introduce el seccion del producto");
				String seccion=input.nextLine();
				System.out.println("Introduce el precio del producto");
				float precio=input.nextFloat();
				misDatos.actualizar(nombre,  proveedor, seccion, precio);
				misDatos.seleccionar();
				input.nextLine();
				break;
			case 5:
				String eliminarOtro = "";
				do {
				System.out.println("Introduce el nombre del producto");
				nombre=input.nextLine();
				misDatos.eliminar(nombre);
				misDatos.seleccionar();
				System.out.println("Quieres eliminar otro producto? Introduce si o no");
				eliminarOtro = input.nextLine();
				} while (eliminarOtro.toLowerCase().contentEquals("si"));

				break;
			case 6:
				misDatos.desconectar();
				break;
			case 7:
				System.out.println("EL PROGRAMA VA A FINALIZAR");
				System.exit(0);
				break;
			}
		} while (op < 7);

	}

	public static int menu() {
		Scanner input = new Scanner(System.in);
		int opcion = 0;
		boolean error = false;
		do {
			try {
				System.out.println("MENU");
				System.out.println("1.- Conectar con la Base de datos");
				System.out.println("2.- Seleccionar la base de datos");
				System.out.println("3.- Inserta datos leidos por pantalla en la base de datos");
				System.out.println("4.- Actualiza datos leidos por teclado en la base de datos");
				System.out.println("5.- Elimina datos de la base de datos");
				System.out.println("6.- Desconectar de la base de datos");
				System.out.println("7.- Salir");
				System.out.println(" ");
				System.out.println("Elegir opcion");
				opcion = input.nextInt();
				if (opcion < 1 || opcion > 7) {
					System.out.println("Error, el valor debe estar entre 1 y 7");
					error = true;
				} else {
					error = false;
				}
			} catch (NumberFormatException e) {
				System.out.println("Error, debes introducir un nimero");
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Comprueba tu conexion al teclado");
				System.exit(0);
			}
		} while (error);
		return opcion;
	}


}
