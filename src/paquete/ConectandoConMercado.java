package paquete;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConectandoConMercado {

	private Connection conexion = null;
	PreparedStatement sentencia = null;

	public void conectar() {
		try {
			String servidor = "jdbc:mysql://localhost:3306/";
			String bbdd = "supermercado";
			String user = "root";
			String password = "";

			conexion = DriverManager.getConnection(servidor + bbdd, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void seleccionar() {
		String sentenciaSql = "SELECT * FROM producto";
		try {
			sentencia = conexion.prepareStatement(sentenciaSql);

			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				System.out.println(resultado.getString(1) + ", " + resultado.getString(2) + ", "
						+ resultado.getString(3) + ", " + resultado.getString(4) + ", " + resultado.getFloat(5));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void insertar(String nombre, String proveedor, String seccion, float precio) {
		try {
			String sentenciaSql = "INSERT INTO producto(nombre, proveedor, seccion, precio) " + "values(?,?,?,?)";
			PreparedStatement sentencia;

			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, nombre);
			sentencia.setString(2, proveedor);
			sentencia.setString(3, seccion);
			sentencia.setFloat(4, precio);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void actualizar(String nombre, String proveedor, String seccion, float precio) {
		try {
			String sentenciaSql = "UPDATE producto set " + "proveedor=?, seccion=?, precio=? WHERE nombre=?";
			PreparedStatement sentencia;

			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, proveedor);
			sentencia.setString(2, seccion);
			sentencia.setFloat(3, precio);
			sentencia.setString(4, nombre);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void eliminar(String nombre) {
		try {
			String sentenciaSql = "DELETE FROM producto WHERE nombre=?";
			PreparedStatement sentencia;

			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, nombre);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void desconectar() throws SQLException {
		sentencia.close();
	}

}
